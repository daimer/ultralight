<?php

/*
 * (c) Andre Meira
 */

/*
 * First of all, activate the autoload.
 * You don't want to require your class manually
 */
ini_set('display_errors', "On");
require_once __DIR__.'/lib/Ultralight/Autoload.php';

$autoload = new \Ultralight\Autoload();
$autoload->addIncludePath(__DIR__.'/lib');
$autoload->addIncludePath(__DIR__.'/app');
$autoload->start();

$app = new App();
$app->init();
$app->setRequest(\Ultralight\Request::createFromGlobals());
$app->start();
