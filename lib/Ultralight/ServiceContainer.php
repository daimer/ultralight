<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class ServiceContainer {

    private $services = [];

    /**
     *
     *
     */
    public function set($name, $service)
    {
        $this->services[$name] = $service;
        return $this;
    }

    /**
     *
     *
     */
    public function has($name)
    {
        return isset($this->services[$name]);
    }

    /**
     *
     *
     */
    public function get($name)
    {
        if (!$this->has($name)) {
            throw new \Exception("Service not found: ".$name, 1);
        }
        return $this->services[$name];
    }

}

