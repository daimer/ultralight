<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Request {

    protected $request = [];

    protected $uri = [];

    /**
     *
     *
     */
    public function getPath()
    {
        return isset($this->uri['path'])
            ? $this->uri['path']
            : '';
    }

    /**
     *
     *
     */
    public function getHost()
    {
        return isset($this->uri['host'])
            ? $this->uri['host']
            : '';
    }

    /**
     *
     *
     */
    public function getScheme()
    {
        return isset($this->uri['scheme'])
            ? $this->uri['scheme']
            : '';
    }

    /**
     *
     *
     */
    public function getBaseHttp()
    {
        return $this->getScheme().'://'.$this->getHost();
    }

    /**
     *
     *
     */
    public function get($name, $default = null)
    {
        return isset($this->request[$name])
            ? $this->request[$name]
            : $default;
    }

    /**
     *
     *
     */
    public static function createFromGlobals()
    {
        $request = new static();
        $request->request = $_POST + $_GET + $_COOKIE;
        $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https://' : 'http://';
        $request->uri = parse_url($scheme.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        return $request;
    }
}

