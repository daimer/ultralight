<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Db {

    protected $pdo = [];

    protected static $instance;

    /**
     * Singleton!
     *
     */
    private function __construct()
    {

    }

    /**
     *
     *
     */
    public function addConnection($name, $pdo)
    {
        $this->pdo[$name] = $pdo;
        return $this;
    }

    /**
     *
     *
     */
    public function addDefaultConnection($pdo)
    {
        $this->pdo['default'] = $pdo;
        return $this;
    }

    /**
     *
     *
     */
    public function getDb($name = 'default')
    {
        if (!isset($this->pdo[$name])) {
            throw new \Exception("No connection: ".$name, 1);
        }
        return $this->pdo[$name];
    }

    /**
     *
     *
     */
    public function addRouter($name, $router)
    {
        $this->routers[$name] = $router;
        return $this;
    }

    /**
     *
     *
     */
    public static function createAndConnect($link, $user, $password)
    {
        $pdo = new \PDO($link, $user, $password);
        return static::getInstance()->addDefaultConnection($pdo);
    }

    /**
     *
     *
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            $me = new static();
            static::$instance = $me;
        }

        return static::$instance;
    }
}

