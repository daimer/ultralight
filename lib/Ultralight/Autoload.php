<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Autoload {

    /**
     *
     *
     */
    public function addIncludePath($path)
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
        return $this;
    }

    /**
     *
     *
     */
    public function start()
    {
        spl_autoload_register(function ($className) {
            $path = str_replace('\\', '/', $className);
            require_once $path.'.php';
        });
    }
}

