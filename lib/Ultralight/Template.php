<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Template {

    protected $datas = [];

    protected $blocks = [];

    protected $output = '';

    protected $basePath = './';

    /**
     *
     *
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
        return $this;
    }

    /**
     *
     *
     */
    public function setDatas(array $datas)
    {
        $this->datas = $datas + $this->datas;
        return $this;
    }

    /**
     *
     *
     */
    public function render($template)
    {
        $datas  = $this->datas;
        $blocks = $this->blocks;
        ob_start();
        include $this->basePath.'/'.str_replace('.', '/', $template).'.php';
        $this->output = ob_get_clean();
        return $this;
    }

    /**
     *
     *
     */
    public function layout($name, \Closure $callback)
    {
        $callback($this);
        return $this->render($name);
    }

    /**
     *
     *
     */
    public function partial($blockName, $template)
    {
        $content = $this->render($template)->output;
        $this->blocks[$blockName] = $content;
        $this->cleanOutput();
        return $this;
    }

    /**
     *
     *
     */
    public function flush()
    {
        echo $this->output;
        $this->cleanOutput();
    }

    /**
     *
     *
     */
    protected function cleanOutput()
    {
        $this->output = '';
        return $this;
    }

    /**
     *
     *
     */
    public static function createFromGlobals()
    {
        $request = new static();
        $request->request = $_POST + $_GET + $_COOKIES;
        $request->uri = parse_url($_SERVER['REQUEST_URI']);
        return $request;
    }
}

