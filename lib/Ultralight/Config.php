<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Config {

    protected $config = [];

    /**
     *
     *
     */
    public function __get($name)
    {
        return isset($this->config[$name])
            ? $this->config[$name]
            : null;
    }
}

