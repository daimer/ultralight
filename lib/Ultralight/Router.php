<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Router {

    protected $basePath = '.';

    protected $request;

    protected $pdo;

    protected $matches = [];

    protected $currentRoute;

    protected $service = null;

    private $regexpRoutes = [];

    protected  $routes = [

    ];

    /**
     *
     *
     */
    public function __construct()
    {
        $this->buildRegexRoutes();
    }

    /**
     * Init the router
     *
     */
    public function init($request, $service = null)
    {
        $this->matches = [];
        $this->currentRoute = null;
        $this->request = $request;
        $this->service = $service;
        $this->findRoute();
        return $this;
    }

    /**
     *
     *
     */
    public function hasMatchingRoute()
    {
        return (bool) $this->currentRoute;
    }

    /**
     * Handle the request
     *
     */
    public function handle()
    {
        if ($this->currentRoute) {
            $method = $this->currentRoute;
            $this->callAction($method);
        }

        return $this;
    }

    /**
     *
     *
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
        return $this;
    }

    /**
     *
     *
     */
    public function redirect($url)
    {
        header('Location: '.$url);
        return $this;
    }

    /**
     *
     *
     */
    public function defaultAction()
    {
        echo "Not found";
    }

    /**
     *
     *
     */
    protected function findRoute()
    {
        $matches = [];

        foreach ($this->regexpRoutes as $regexp => $method) {
            if (preg_match($regexp, $this->request->getPath(), $matches)) {
                $this->matches = $matches;
                $this->currentRoute = $method;
                return $this;
            }
        }

        return $this;
    }

    /**
     *
     *
     */
    protected function getUrlParam($name)
    {
        return isset($this->matches[$name])
            ? $this->matches[$name]
            : null;
    }

    /**
     *
     *
     */
    protected function callAction($method)
    {
        $action = $method.'Action';
        $this->{$action}();
        return $this;
    }

    /**
     *
     *
     */
    protected function render($template, $data)
    {
        $data = $data;
        include $this->basePath.'/'.str_replace('.', '/', $template).'.php';
    }

    /**
     *
     *
     */
    protected function service($name)
    {
        if (!$this->service) {
            throw new \Exception("No service for this router", 1);
        }

        return $this->service->get($name);
    }

    /**
     *
     *
     */
    protected function buildRegexRoutes()
    {
        foreach ($this->routes as $r => $m) {
            $this->regexpRoutes[$this->buildRegexRoute($r)] = $m;
        }
        return $this;
    }

    /**
     *
     *
     */
    protected function buildRegexRoute($route)
    {
        $newParts = [];
        $parts = explode('/', $route);

        foreach ($parts as $part) {
            $newParts[] = $this->convertToRegexExp($part);
        }

        return '|^'.implode('/', $newParts).'$|';
    }

    /**
     *
     *
     */
    protected function convertToRegexExp($part)
    {
        $part = str_replace('{', '(?<', $part);
        $part = str_replace('}', '>.*)', $part);
        return $part;
    }

    /**
     *
     *
     */
    public static function create()
    {
        return new static();
    }
}

