<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class Session {

    /**
     *
     *
     */
    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    /**
     *
     *
     */
    public function remove($key)
    {
        unset($_SESSION[$key]);
        return $this;
    }

    /**
     *
     *
     */
    public function get($key, $default = null)
    {
        return $this->has($key)
            ? $_SESSION[$key]
            : $default;
    }

    /**
     *
     *
     */
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
        return $this;
    }

    /**
     *
     *
     */
    public function regenerateId()
    {
        session_regenerate_id();
        return $this;
    }

    /**
     *
     *
     */
    public static function start()
    {
        session_start();
        return new static;
    }
}

