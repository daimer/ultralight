<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Ultralight;

class App {

    protected $routers = [];

    protected $request = [];

    protected $service = null;

    /**
     *
     *
     */
    public function init($service = null)
    {
        $service = $service ? $service : new ServiceContainer();
        $this->service = $service;
        return $this;
    }

    /**
     *
     *
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     *
     *
     */
    public function addRouter($name, $router)
    {
        $this->routers[$name] = $router;
        return $this;
    }

    /**
     *
     *
     */
    public function addDefaultRouter($router)
    {
        $this->routers['default'] = $router;
        return $this;
    }

    /**
     *
     *
     */
    public function start()
    {
        if (!isset($this->routers['default'])) {
            throw new \Exception("Everybody needs a default router", 1);
        }

        foreach ($this->routers as $router) {
            $router->init($this->request, $this->service);
            if ($router->hasMatchingRoute()) {
                return $router->handle();
            }
        }

        return $this->routers['default']->defaultAction();
    }
}

