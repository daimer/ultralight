<?php

use Ultralight\App as BaseConf;

/***************************************************************
 * README
 *
 * This is a dist file
 * Replace <dbname>, <username>, <pswd> with you own values
 ***************************************************************/

/**
 *
 */
class Config extends BaseConf
{

    protected $config = [
        'dburi'  => 'mysql:host=127.0.0.1;dbname=<dbname>',
        'dbuser' => '<username>',
        'dbpswd' => '<pswd>',
    ];
}
