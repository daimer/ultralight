<?php

use Ultralight\App as BaseApp;

/**
*
*/
class App extends BaseApp
{

    public function init($service = null)
    {
        parent::init();
        $conf = new Config();

        //@todo user config for that
        $db = \Ultralight\Db::createAndConnect($conf->dburi, $conf->dbuser, $conf->dbpswd);
        $this->service->set('db', $db);

        $template = new \Ultralight\Template();
        $this->service->set('tpl', $template->setBasePath(__DIR__.'/views'));

        $session = \Ultralight\Session::start();
        $this->service->set('session', $session);


        $router = new Routers\Main();
        $this->addDefaultRouter($router);

        return $this;
    }
}
