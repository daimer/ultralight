<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>{% block title %}BBK links!{% endblock %}</title>

        <link href="/style/reset.css" rel="stylesheet" media="screen" />
        <link href="/style/main.css" rel="stylesheet" media="screen" />
        <link rel="icon" type="image/x-icon" href="" />
    </head>
    <body>
        <div id="content">
        <p id="connect-text">
            Link not found.
        </p>
    </div>
    </body>
</html>
