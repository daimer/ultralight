<html>
<head>
<title><?php echo $title ?></title>
</head>
<body>
<h1>List of Posts</h1>
<ul>
    <?php foreach ($datas['posts'] as $post): ?>
    <li>
        <a href="/read?id=<?php echo $post['id'] ?>">
            <?php echo $post['title'] ?>
        </a>
    </li>
    <?php endforeach; ?>
    </ul>
</body>
</html>
