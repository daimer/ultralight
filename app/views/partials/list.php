<div id="create-link">
        <form action="/create-link" method="post">
            <input type="text" placeholder="deep link" name="target"  value=""/>
            <input type="text" placeholder="comment"   name="comment" value=""/>
            <input type="submit" value="create link"   name="submit"  class="submit"/>
        </form>
    </div>

    <div id="content">
        <?php if ($datas['links']): ?>
            <div id="list-links">

                <?php foreach ($datas['links'] as $link): ?>

                    <p>deep link: <?php echo htmlentities($link['target']);?></p>
                    <p>gen link: <?php echo htmlentities($datas['domain']);?>/r/<?php echo htmlentities($link['hash']);?></p>

                    <a href="/delete/<?php echo htmlentities($link['id']);?>">delete</a>
                    <a href="/renew/<?php echo htmlentities($link['id']);?>">renew</a>
                    <hr />
                <?php endforeach; ?>
            </div>
        <?php else: ?>
        <p id="connect-text">
            You have no links.
        </p>
        <?php endif; ?>
    </div>
