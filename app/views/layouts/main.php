<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>BBK links!</title>

        <link href="/style/reset.css" rel="stylesheet" media="screen" />
        <link href="/style/main.css" rel="stylesheet" media="screen" />
        <link rel="icon" type="image/x-icon" href="" />
    </head>
    <body>
        <?php echo $blocks['header']; ?>
        <?php echo $blocks['body']; ?>
    </body>
</html>
