<?php
/*
 * This file is part of the Ultralight package.
 *
 * (c) Andre Meira
 *
 */

namespace Routers;

use Ultralight\Router as BaseRouter;

class Main extends BaseRouter {

    protected  $routes = [
        ''              => 'index',
        '/'             => 'index',
        '/list'         => 'list',
        '/connect'      => 'connect',
        '/renew/{id}'   => 'renew',
        '/create-link'  => 'createLink',
        '/delete/{id}'  => 'delete',
        '/registration' => 'registration',
        '/r/{hash}'      => 'rebounce',
        '/logout'       => 'logout',
        '/confirm-registration' => 'confirmRegistration',



    ];


    public function testAction()
    {
        $this->service('tpl')->setDatas([
            'posts' => [
                ['id'=>0, "title"=>"post-1"],
                ['id'=>0, "title"=>"post-1"],
            ]
        ])->render('test')->flush();
    }


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if ($this->service('session')->has('user')) {
            return $this->redirect('/list');
        }

        $this->service('tpl')->layout('layouts.main', function ($t) {
            $t->partial('header', 'partials.header');
            $t->partial('body', 'partials.index');
        })->flush();
    }

    /**
     * @Route("/connect", name="connect")
     */
    public function connectAction()
    {
        if (!$this->request->get('login')
        || !$this->request->get('pswd')) {
            return $this->redirect('/');
        }

        $this->service('session')->regenerateId();
        $stmt = $this->service('db')->getDb()->prepare('select * from user where login = :login');
        $stmt->execute(['login' => $this->request->get('login')]);
        $result = $stmt->fetchAll();

        if ($result && $result[0]['password'] !== $this->request->get('pswd')) {
            return $this->redirect('/');
        }

        if (!$result) {
            $this->service('session')->set('registration', [
                'login'    => $this->request->get('login'),
                'password' => $this->request->get('pswd'),
            ]);
            return $this->redirect('/registration');
        }

        $this->service('session')->set('user', [
            'id'       => $result[0]['id'],
            'login'    => $result[0]['login'],
            'password' => $result[0]['password'],
        ]);
        return $this->redirect('/list');
    }

    /**
     * @Route("/registration", name="registration")
     */
    public function registrationAction()
    {
        if (!$this->service('session')->has('registration')) {
            return $this->redirect('/');
        }

        $this->service('tpl')->layout('layouts.main', function ($t) {
            $t->partial('header', 'partials.header');
            $t->partial('body', 'partials.registration');
        })->flush();
    }

    /**
     * @Route("/confirm-registration", name="confirm-registration")
     */
    public function confirmRegistrationAction()
    {
        if (!$this->service('session')->has('registration')) {
            return $this->redirect('/');
        }

        $data = $this->service('session')->get('registration');
        $stmt = $this->service('db')->getDb()->prepare('insert into user (login, password) VALUES (:login, :password)');
        $stmt->execute(['login' => $data['login'], 'password' => $data['password']]);
        $this->service('session')->set('user', $data + ['id' => $this->service('db')->getDb()->lastInsertId()]);

        $this->service('session')->remove('registration');
        return $this->redirect('/list');
    }

    /**
     * @Route("/list", name="list")
     */
    public function listAction()
    {
        if (!$this->service('session')->has('user')) {
            return $this->redirect('/');
        }

        $stmt = $this->service('db')->getDb()->prepare('select * from link where user_id = :uid');
        $stmt->execute(['uid' => $this->service('session')->get('user')["id"]]);
        $links = $stmt->fetchAll();

        $this->service('tpl')->setDatas([
            'links'  => $links,
            'domain' => $this->request->getBaseHttp(),
            'user'   => $this->service('session')->get('user')
        ]);

        $this->service('tpl')->layout('layouts.main', function ($t) {
            $t->partial('header', 'partials.header-connected');
            $t->partial('body', 'partials.list');
        })->flush();
    }

    /**
     * @Route("/create-link", name="create-link")
     */
    public function createLinkAction()
    {
        $user = $this->service('session')->get('user');
        if (!$this->request->get('target') || !isset($user['id'])) {
            return $this->redirect('/list');
        }

        $stmt = $this->service('db')->getDb()->prepare('insert into link (
                target, comment, date_update, hash, user_id
            ) VALUES (:target, :comment, :date_update, :hash, :user_id)'
        );
        $stmt->execute([
            'target'  => $this->request->get('target'),
            'comment' => $this->request->get('comment'),
            'date_update' => time(),
            'hash'        => hash('sha1', uniqid()),
            'user_id'     => $user['id']
        ]);

        return $this->redirect('/list');
    }

    /**
     * @Route("/renew/{id}", name="renew")
     */
    public function renewAction()
    {
        if (!$this->service('session')->has('user')) {
            $this->redirect('/');
        }

        $id = $this->getUrlParam('id');
        $stmt = $this->service('db')->getDb()->prepare('select * from link where id = :lid');
        $stmt->execute(['lid' => $id]);
        $link = $stmt->fetchAll();

        if (!$link || $link[0]['user_id'] != $this->service('session')->get('user')['id']) {
            return $this->redirect('/');
        }

        $stmt = $this->service('db')->getDb()->prepare('UPDATE link set hash=:hash, date_update=:date where id=:lid limit 1');
        $stmt->execute(['hash' => hash('sha1', uniqid()), 'lid' => $id, 'date' => time()]);

        return $this->redirect('/');
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction()
    {
        if (!$this->service('session')->has('user')) {
            $this->redirect('/');
        }

        $id   = $this->getUrlParam('id');
        $stmt = $this->service('db')->getDb()->prepare('select * from link where id = :lid');
        $stmt->execute(['lid' => $id]);
        $link = $stmt->fetchAll();

        if (!$link || $link[0]['user_id'] != $this->service('session')->get('user')['id']) {
            return $this->redirect('/list');
        }

        $stmt = $this->service('db')->getDb()->prepare('DELETE FROM link where id=:lid limit 1');
        $stmt->execute(['lid' => $id]);

        return $this->redirect('/list');
    }

    /**
     * @Route("/r/{hash}", name="redirect")
     */
    public function rebounceAction()
    {
        $stmt = $this->service('db')->getDb()->prepare('select * from link where hash = :hash');
        $stmt->execute(['hash' => $this->getUrlParam('hash')]);
        $link = $stmt->fetchAll();

        if (!$link) {
            return $this->service('tpl')->render('partial.not-found.html');
        }

        if (($link[0]['date_update'] + 60 * 60 * 2) < time()) {
            return $this->service('tpl')->render('partial.not-found.html');
        }

        return $this->redirect($link[0]['target']);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        $this->service('session')->remove('user');
        $this->service('session')->regenerateId();
        return $this->redirect('/');
    }
}

